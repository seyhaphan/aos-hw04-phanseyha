package com.kshrd.homework004;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class HomeFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment,container,false);
        Button btnDetail = view.findViewById(R.id.btnDetail);

        if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
           btnDetail.setVisibility(View.GONE);
        }

        btnDetail.setOnClickListener(v->{
              getFragmentManager().beginTransaction()
              .replace(R.id.home_container,new DetailFragment())
              .addToBackStack(null)
              .commit();
        });

        return view;
    }
}
