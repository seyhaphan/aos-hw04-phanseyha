package com.kshrd.homework004;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.MyViewHolder> {

    private EmailModel[] emailList;
    private RecyclerViewAction recyclerViewAction;

    public MyListAdapter(EmailModel[] emailList) {
        this.emailList = emailList;
    }

    public MyListAdapter(EmailModel[] emailList, RecyclerViewAction recyclerViewAction) {
        this.emailList = emailList;
        this.recyclerViewAction = recyclerViewAction;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.my_list,parent,false);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        EmailModel emailModel = emailList[position];
        holder.tvEmail.setText(emailModel.getEmail());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewAction.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return emailList.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmail;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEmail = itemView.findViewById(R.id.tvEmail);
        }
    }
}
