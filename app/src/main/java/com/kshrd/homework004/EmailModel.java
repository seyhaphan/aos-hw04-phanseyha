package com.kshrd.homework004;

import android.widget.ArrayAdapter;

import java.util.ArrayList;

public class EmailModel {
    String email;

    public EmailModel() {
    }

    static EmailModel[] EmilList(){
        EmailModel[] emailList = new EmailModel[20];
        for (int i = 0; i < emailList.length; i++){
            emailList[i] = new EmailModel("User000"+i+"@gmail.com");
        }
        return emailList;
    }

    public EmailModel(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
