package com.kshrd.homework004;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DetailFragment extends Fragment implements RecyclerViewAction {

    RecyclerView recyclerView;
    EmailModel[] emailList = EmailModel.EmilList();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment,container,false);

        recyclerView = view.findViewById(R.id.myRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter( new MyListAdapter(emailList,this));

        return view;
    }

    @Override
    public void onItemClick(int position) {
       Toast.makeText(getActivity().getApplicationContext(),emailList[position].getEmail(), Toast.LENGTH_SHORT).show();
    }
}
