package com.kshrd.homework004;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int orientation = getResources().getConfiguration().orientation;

        if(orientation == Configuration.ORIENTATION_PORTRAIT){

            getSupportFragmentManager().beginTransaction().replace(R.id.home_container,new HomeFragment()).commit();

        }else if(orientation == Configuration.ORIENTATION_LANDSCAPE){

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.left_fragment,new HomeFragment());
            fragmentTransaction.replace(R.id.right_fragment,new DetailFragment());
            fragmentTransaction.commit();

        }

    }

}