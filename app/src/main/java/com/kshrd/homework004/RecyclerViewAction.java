package com.kshrd.homework004;

public interface RecyclerViewAction {
    void onItemClick(int position);
}
